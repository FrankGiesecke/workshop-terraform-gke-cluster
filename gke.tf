variable "gke_username" {
  default     = ""
  description = "gke username"
}

variable "gke_password" {
  default     = ""
  description = "gke password"
}

# GKE cluster
resource "google_container_cluster" "workshop" {
  name     = "workshop"
  location = var.gcp_region

  enable_autopilot = true
}
