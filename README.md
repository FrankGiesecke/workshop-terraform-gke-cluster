# Workshop Terraform on Google Cloud Platform

This project demonstrate how to use terraform to provision a kubernetes cluster (GKE) on Google Cloud Platform.

## Prerequisites

To interact with terraform and GCP in a CI, it is necessary to use a service account.  
The service account credential file must be supplied as a file. The file name must be exposed in the environment variable `GOOGLE_APPLICATION_CREDENTIALS`.

The service account must be configured with some roles to do the job:
- `Compute Viewer`  
  Read-only access to get and list information about all Compute Engine resources, including instances, disks, and firewalls.  
  Allows getting and listing information about disks, images, and snapshots, but does not allow reading the data stored on them.
- `Kubernetes Engine Admin`  
  Full management of Kubernetes Clusters and their Kubernetes API objects.
- `Kubernetes Engine Cluster Viewer`  
  Get and list access to GKE Clusters.
- `Service Account User`  
  Run operations as the service account.
