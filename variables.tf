variable "gcp_project_id" {
  description = "ID of the Google Cloud Project"
  type        = string
}
variable "gcp_region" {
  description = "Name of the Google Cloud Project Region"
  type        = string
}

